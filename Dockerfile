FROM node as build
WORKDIR /app
COPY package*.json ./
RUN npm install
# Copy the main application
COPY . ./
# Arguments
ARG configuration=production
#ARG NODE_OPTIONS=--openssl-legacy-provider yarn dev

# Build the application
RUN npm run build -- --outputPath=./dist/out --configuration $configuration

## Stage 2, production with Nginx
FROM nginx
# Copy the angular build from Stage 1
COPY --from=build /app/dist/out/ /usr/share/nginx/html
# Copy our custom nginx config
COPY /nginx-custom.conf /etc/nginx/conf.d/default.conf
# Expose port 80
EXPOSE 80
ENTRYPOINT ["nginx","-g","daemon off;"]
