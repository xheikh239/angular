import { Component, OnInit } from '@angular/core';
import { Request} from '../request';
import { ActivatedRoute, Router } from '@angular/router';
import { RequestService} from '../request.service';

@Component({
  selector: 'app-update-request',
  templateUrl: './update-request.component.html',
  styleUrls: ['./update-request.component.css']
})
export class UpdateRequestComponent implements OnInit {

  id: number;
  request: Request;

  constructor(private route: ActivatedRoute, private router: Router,
              private requestService: RequestService) { }

  ngOnInit() {
    this.request = new Request();

    this.id = this.route.snapshot.params.id;

    this.requestService.getRequest(this.id)
      .subscribe(data => {
        console.log(data);
        this.request = data;
      }, error => console.log(error));
  }

  updateRequest() {
    this.requestService.updateRequest(this.id, this.request)
      .subscribe(data => {
        console.log(data);
        this.request = new Request();
        this.gotoList();
      }, error => console.log(error));
  }

  onSubmit() {
    this.updateRequest();
  }

  gotoList() {
    this.router.navigate(['/requests']);
  }
}
