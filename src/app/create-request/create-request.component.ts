import { RequestService} from '../request.service';
import { Request} from '../request';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-create-request',
  templateUrl: './create-request.component.html',
  styleUrls: ['./create-request.component.css']
})
export class CreateRequestComponent implements OnInit {

  request: Request = new Request();
  submitted = false;

  constructor(private requestService: RequestService,
              private router: Router) { }

  ngOnInit() {
  }

  newRequest(): void {
    this.submitted = false;
    this.request = new Request();
  }

  save() {
    this.requestService
      .createRequest(this.request).subscribe(data => {
        console.log(data);
        this.request = new Request();
        this.gotoList();
      },
      error => console.log(error));
  }

  onSubmit(value: any) {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['/requests']);
  }

 /* RequestForm = new FormGroup({
    name: new FormControl('',Validators.required),
    matricule: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    raison: new FormControl('', Validators.required),
    datDebut: new FormControl('', Validators.required),
    dateFin: new FormControl('', Validators.required)

  })

  get matricule() { return this.RequestForm.get('matricule')}
  get name(){ return this.RequestForm.get('name')}
  get email(){ return this.RequestForm.get('email')}
  get raison(){ return this.RequestForm.get('raison')}
  get dateDebut(){ return this.RequestForm.get('dateDebut')}
  get dateFin(){ return this.RequestForm.get('dateFin')}
  */
}

