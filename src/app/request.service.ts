import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  private apiServer1Url = environment.apiBaseUrl;
  private apiServer2Url = environment.apiBaseUrl1;
  constructor(private http: HttpClient) { }

  getRequest(id: number): Observable<any> {
    return this.http.get(`${this.apiServer1Url}/user/req/find/${id}`);
  }

  createRequest(request: object): Observable<object> {
    return this.http.post(`${this.apiServer1Url}/user/req/add`, request);
  }

  updateRequest(id: number, value: any): Observable<object> {
    return this.http.put(`${this.apiServer1Url}/user/req/update/${id}`, value);
  }

  deleteRequest(id: number): Observable<any> {
    return this.http.delete(`${this.apiServer1Url}/user/req/delete/${id}`, { responseType: 'text' });
  }

  getRequestsList(): Observable<any> {
    return this.http.get(`${this.apiServer1Url}/user/req/list`);
  }

  myRequests(): Observable<any> {
    return this.http.get(`${this.apiServer1Url}/user/req/my-requests/`);
  }

  manageRequests(): Observable<any> {
    return this.http.get(`${this.apiServer1Url}/user/req/manage-requests/`);
  }

  acceptRequest(id: number): Observable<any> {
    return this.http.get(`${this.apiServer2Url}/accept/${id}`);
  }

  rejectRequest(id: number): Observable<any> {
    return this.http.get(`${this.apiServer2Url}/reject/${id}`);
  }

}
