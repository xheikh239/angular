import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {Request} from '../request';
import {RequestService} from '../request.service';
import {Router} from '@angular/router';
import {AuthenticationService} from '../login/auth.service';

@Component({
  selector: 'app-manage-pending-requests',
  templateUrl: './manage-pending-requests.component.html',
  styleUrls: ['./manage-pending-requests.component.css']
})

export class ManagePendingRequestsComponent implements OnInit {
  requests: Observable< Request[]>;

  constructor(private requestService: RequestService, private router: Router) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.requests = this.requestService.manageRequests();
  }

  deleteRequest(id: number) {
    this.requestService.deleteRequest(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  acceptRequest(id: number) {
    this.requestService.acceptRequest(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  rejectRequest(id: number) {
    this.requestService.rejectRequest(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  requestDetails(id: number) {
    this.router.navigate(['details', id]);
  }

  updateRequest(id: number) {
    this.router.navigate(['update', id]);
  }
}

