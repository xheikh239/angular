import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {Request} from '../request';
import {RequestService} from '../request.service';

@Component({
  selector: 'app-my-requests',
  templateUrl: './my-requests.component.html',
  styleUrls: ['./my-requests.component.css']
})
export class MyRequestsComponent implements OnInit {

  requests: Observable<Request[]>;

  constructor(private requestService: RequestService) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.requests = this.requestService.myRequests();
  }
}

