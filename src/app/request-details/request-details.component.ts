import { Component, OnInit } from '@angular/core';
import { Request} from '../request';
import { RequestService} from '../request.service';
import { RequestListComponent } from '../request-list/request-list.component';
import { Router, ActivatedRoute } from '@angular/router';
import {AuthenticationService} from '../login/auth.service';

@Component({
  selector: 'app-request-details',
  templateUrl: './request-details.component.html',
  styleUrls: ['./request-details.component.css']
})
export class RequestDetailsComponent implements OnInit {

  id: number;
  request: Request;

  constructor(private route: ActivatedRoute, private router: Router,
              private requestService: RequestService, public auth: AuthenticationService ) { }

  ngOnInit() {
    this.request = new Request();

    this.id = this.route.snapshot.params.id;

    this.requestService.getRequest(this.id)
      .subscribe(data => {
        console.log(data);
        this.request = data;
      }, error => console.log(error));
  }

  list() {
    this.router.navigate(['requests']);
  }
}
