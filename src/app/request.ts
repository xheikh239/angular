export class Request {
  id: number;
  matricule: string;
  userName: string;
  email: string;
  raison: string;
  dateDebut: Date;
  dateFin: Date;
  duration: number;
  active: boolean;
  accept: boolean;
  createdAt: Date;
}
