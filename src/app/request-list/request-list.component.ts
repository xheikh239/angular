import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import { Request} from '../request';
import {RequestService} from '../request.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-request-list',
  templateUrl: './request-list.component.html',
  styleUrls: ['./request-list.component.css']
})
export class RequestListComponent implements OnInit {
  requests: Observable<Request[]>;

  constructor(private requestService: RequestService, private router: Router) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.requests = this.requestService.getRequestsList();
  }
}

