import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestListComponent} from './request-list/request-list.component';
import { LoginComponent } from './login/login.component';
import {CreateRequestComponent} from './create-request/create-request.component';
import {ManagePendingRequestsComponent} from './manage-pending-requests/manage-pending-requests.component';
import {MyRequestsComponent} from './my-requests/my-requests.component';
import {RequestDetailsComponent} from './request-details/request-details.component';
import {UpdateRequestComponent} from './update-request/update-request.component';
import {AuthGuard} from './guard/auth.guard';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: '', component: LoginComponent},
  {path: 'requests', component: RequestListComponent, canActivate: [AuthGuard]},
  {path: 'add', component: CreateRequestComponent, canActivate: [AuthGuard]},
  {path: 'manage-requests', component: ManagePendingRequestsComponent, canActivate: [AuthGuard] },
  {path: 'my-requests', component: MyRequestsComponent, canActivate: [AuthGuard]},
  {path: 'details/:id', component: RequestDetailsComponent, canActivate: [AuthGuard]},
  {path: 'update/:id', component: UpdateRequestComponent, canActivate: [AuthGuard]},
  {path: 'logout', component: LoginComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
